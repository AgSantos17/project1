import React, { Component } from 'react';
import Main from './Main';
import Headers from './Header';
import '../assets/style.css';
import { Container } from 'semantic-ui-react';


class IndexPage extends Component {

  render() {

    return (
      <div className="App">

        <Headers />
          <br/>
      <Container>
        <Main />
      </Container>
      </div>
    );

  }
}

export default IndexPage;
