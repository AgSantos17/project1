import React, { Component } from 'react';

class Checkout extends Component {

  render() {
    return (
      <div>
        <button onClick={this.props.checkout}>Checkout</button>
      </div>
    );
  }
}

export default Checkout;
