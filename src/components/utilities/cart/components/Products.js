import React, { Component } from 'react';
import MaterialApi from '../../../../data/material-api';
import { Button } from 'semantic-ui-react';
import matchSorter from 'match-sorter';
import '../../../../assets/style.css';

// Import React Table
import ReactTable from "react-table";

class Products extends Component {

  constructor(){
    super();
    this.categories = [];
    this.productsArray = [{
      category : '',
      materials: []
    }];
  }

  selectProduct(item){
    this.props.addProduct(item);
  }

  mountTable(category){
      return (
        <ReactTable
          data={MaterialApi.all()}
          filterable
          defaultFilterMethod={(filter, row) =>
            String(row[filter.id]) === filter.value}
          noDataText="No Data"
          columns={[
            {
              Header: "Materials",
              columns: [
                {
                  Header: "Brand",
                  id: "brand",
                  accessor: d => d.brand,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["brand"] }),
                  filterAll: true
                },
                {
                  Header: "Category",
                  id: "category",
                  accessor: d => d.category,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["category"] }),
                  filterAll: true
                },
                {
                  Header: "Sub-category",
                  id: "subcategory",
                  accessor: d => d.subcategory,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["subcategory"] }),
                  filterAll: true
                },
                {
                  Header: "Description",
                  id: "description",
                  accessor: d => d.description,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["description"] }),
                  filterAll: true
                },
                {
                  Header: "Color",
                  id: "color",
                  accessor: d => d.color,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["color"] }),
                  filterAll: true
                },
                {
                  Header: "Package",
                  id: "package",
                  accessor: d => d.package,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["package"] }),
                  filterAll: true
                },
                {
                  Header: "Measurement",
                  id: "measurement",
                  accessor: d => d.measurement,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["measurement"] }),
                  filterAll: true
                },
                {
                  Header: "Price",
                  id: "price",
                  accessor: d => '₱'+d.price.toFixed(2),
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["price"] }),
                  filterAll: true
                }]
              },
          {
            Header: "Action",
            columns: [
              {
                id: "key",
                accessor: "key",
                  Cell: ({value,original}) => (<Button onClick={this.selectProduct.bind(this,original)}>+</Button>),
                Filter: ({ filter, onChange }) =>
                <div />
              },
            ]
            }
          ]}
          defaultPageSize={5}
          className="center"
        />

        );
    }

  render() {
    return (
      <div>
          {this.mountTable()}
      </div>
    );
  }
}

export default Products;
