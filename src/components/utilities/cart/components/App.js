import React, { Component } from 'react';
import Products from './Products';
import Cart from './Cart';

class App extends Component {
  constructor() {
    super();
    this.state = {
      materials: [],
      totals:0,
    };
    this.reset = this.reset.bind(this);
  }

  addProduct(material){
    let newA = this.state.materials;
    let existingProduct = newA.find((p) => {
        return p.key === material.key;
    });

    if (existingProduct){
        existingProduct.qtd = existingProduct.qtd + 1 || 1;
        existingProduct.total = existingProduct.qtd * JSON.parse(existingProduct.price);
    } else {
      if(material.total === 0 || material.total === undefined) {
      material.total = JSON.parse(material.price);
      }
      newA.push(material);
    }

    var totals =  0;
    for(var i=0;i<this.state.materials.length;i++)
      {
        totals = totals + this.state.materials[i]['total'];
       }

    this.setState({
      materials: newA,
      totals: totals.toFixed(2)
    });
    console.log(this.state.totals);
    console.log(this.state.materials);
  }

    doCheckout(){
      console.log(this.state.materials)
    }

    reset() {
      let totals = 0
      let newA = this.state.materials;
      newA.splice(0);
      this.setState({
        materials: newA,
        totals: totals
      });
    }


  removeProduct(material){
    let newA = this.state.materials;
    let existingProduct = newA.find((p) => {
        return p.key === material.key;
    });

    if (existingProduct && existingProduct.qtd > 1){
        existingProduct.qtd = existingProduct.qtd - 1 || 1;
        existingProduct.total = existingProduct.qtd * JSON.parse(existingProduct.price);
    } else {
      if(material.total === 0 || material.total === undefined) {
      material.total = JSON.parse(material.price);
      }

      const index = newA.indexOf(material);
      if (index !== -1) {
          newA.splice(index, 1);
      }

    }

    var totals =  0;
    for(var i=0;i<this.state.materials.length;i++)
      {
        totals = totals + this.state.materials[i]['total'];
       }

    this.setState({
      materials: newA,
      totals: totals.toFixed(2)
    });
    console.log(this.state.totals);
    console.log(this.state.materials);
  }

  render() {
    return (
      <div>
        <div className="App">
          <div className="App-header">
          </div>
        </div>
        <div className="wrapper">
          <Products addProduct={this.addProduct.bind(this)} />
        </div>
          <Cart removeProduct={this.removeProduct.bind(this)} reset={this.reset.bind(this)} products={this.state.materials} totals={this.state.totals} checkout={this.doCheckout.bind(this)}/>
      </div>
    );
  }
}

export default App;
