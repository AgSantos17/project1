import React, { Component } from 'react';
import { Button, Form, Icon } from 'semantic-ui-react';
import matchSorter from 'match-sorter';
import '../../../../assets/style.css';
import { Link } from 'react-router-dom';

// Import React Table
import ReactTable from "react-table";

class Cart extends Component {

  constructor(props){
    super(props);
    this.state = {
      materials: this.props.products,
      totals: this.props.totals
    }
  }

  selectProduct(item){
    this.props.removeProduct(item);
  }

  resetProducts(item){
    this.props.reset(item);
  }

    mountTable(){
      return(
        <ReactTable
          data={this.state.materials}
          filterable
          defaultFilterMethod={(filter, row) =>
            String(row[filter.id]) === filter.value}
          noDataText="No Data"
          columns={[
            {
              Header: "Materials",
              columns: [
                {
                  Header: "Brand",
                  id: "brand",
                  accessor: d => d.brand,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["brand"] }),
                  filterAll: true
                },
                {
                  Header: "Category",
                  id: "category",
                  accessor: d => d.category,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["category"] }),
                  filterAll: true
                },
                {
                  Header: "Sub-category",
                  id: "subcategory",
                  accessor: d => d.subcategory,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["subcategory"] }),
                  filterAll: true
                },
                {
                  Header: "Description",
                  id: "description",
                  accessor: d => d.description,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["description"] }),
                  filterAll: true
                },
                {
                  Header: "Color",
                  id: "color",
                  accessor: d => d.color,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["color"] }),
                  filterAll: true
                },
                {
                  Header: "Package",
                  id: "package",
                  accessor: d => d.package,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["package"] }),
                  filterAll: true
                },
                {
                  Header: "Measurement",
                  id: "measurement",
                  accessor: d => d.measurement,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["measurement"] }),
                  filterAll: true
                },
                {
                  Header: "Quantity",
                  id: "qty",
                  accessor: d => d.qtd,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["qtd"] }),
                  filterAll: true
                },
                {
                  Header: "Price",
                  id: "price",
                  accessor: d => '₱'+d.total.toFixed(2),
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["total"] }),
                  filterAll: true
                }]
              },
          {
            Header: "Action",
            columns: [
              {
                id: "key",
                accessor: "key",
                  Cell: ({value,original}) => (<Button onClick={this.selectProduct.bind(this,original)}>-</Button>),
                Filter: ({ filter, onChange }) =>
                <div />
              },
            ]
            }
          ]}
          defaultPageSize={5}
          className="center"
        />
      );
    }

  render() {
    return (
      <div>
        <h3 className="title center"><Icon fitted name='angle double down' size='huge'/></h3>
          {this.mountTable()}
          <br/><br/>
          <Form >
            <Form.Field className="center">
              <label>Remaining balance:</label>
               <div className='ui transparent input'>
                <input className="center" placeholder='First Name' value={'₱' + this.props.totals}/>
               </div>
            </Form.Field>
            <Form.Field>
              <Button as={Link} to="/quotation">Back</Button>
              <Button className="f-right" color="red" onClick={this.resetProducts.bind(this)}>Reset</Button>
              <Button className="f-right" color="green">Submit Cart</Button>
            </Form.Field>
          </Form>
      </div>
    );
  }
}

export default Cart;
