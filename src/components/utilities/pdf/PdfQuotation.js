import React from 'react';
import jsPDF from 'jspdf';
import html2pdf from 'html2pdf.js';
import { Button} from 'semantic-ui-react';

class PdfQuotation extends React.Component {

  pdfToHTML(){
    var pdf = new jsPDF('p', 'pt', 'letter');
    var canvas = pdf.canvas;
    canvas.height = 72 * 11;
    canvas.width= 72 * 8.5;;
    // can also be document.body
    var html = document.getElementById('HTMLtoPDF');
    html2pdf(html, pdf, function(pdf) {
            pdf.output('dataurlnewwindow');
    });
  }

  render() {
    return (
      <div>
        <Button circular color="green" className="f-right" onClick={this.pdfToHTML}>Download PDF</Button>
      </div>
    );
  }
}

export default PdfQuotation;
