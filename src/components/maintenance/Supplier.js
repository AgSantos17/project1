import React, { Component } from 'react';
import { Form, Segment, Divider, Header, Button } from 'semantic-ui-react';

class Supplier extends Component
{
  render()
  {
    return(
      <Segment padded>
        <Form unstackable>
           <Divider horizontal><Header as='h1'>Maintenance: Supplier</Header></Divider>
            <br/>
           <Divider horizontal><Header as='h3'>Company Information</Header></Divider>
            <br/>
          <Form.Group widths='equal'>
            <Form.Input label='Company Name:' placeholder='Company Name' />
            <Form.Input label='Phone Number:' placeholder='Phone Number' />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input label='Fax Number:' placeholder='Fax Number' />
            <Form.Input label='Email Address:' placeholder='Email Address' />
          </Form.Group>
            <br/>
           <Divider horizontal><Header as='h3'>Contact Person</Header></Divider>
            <br/>
          <Form.Group widths='equal'>
            <Form.Input label='First Name:' placeholder='First Name' />
            <Form.Input label='Middle Name:' placeholder='Middle Number' />
            <Form.Input label='Last Name:' placeholder='Last Name' />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input label='Phone Number:' placeholder='Phone Number' />
            <Form.Input label='Mobile Number:' placeholder='Mobile Number' />
          </Form.Group>
            <br/>
           <Divider horizontal><Header as='h3'>Address</Header></Divider>
            <br/>
          <Form.Group widths='equal'>
            <Form.Input label='Street:' placeholder='Street' />
            <Form.Input label='City:' placeholder='City' />
          </Form.Group>
            <br/>
          <div className="center">
            <Button primary>Submit</Button>
          </div>
        </Form>
      </Segment>
    )
  }
}

export default Supplier;
