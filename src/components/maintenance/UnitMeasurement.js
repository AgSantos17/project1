import React, { Component } from 'react';
import { Form, Segment, Divider, Header, Button } from 'semantic-ui-react';

class UnitMeasurement extends Component
{
  render()
  {
    return(
      <Segment padded>
        <Form unstackable>
           <Divider horizontal><Header as='h1'>Maintenance: Unit Measurement</Header></Divider>
            <br/>
              <Form.Input label='Unit Measurement:' placeholder='Unit Measurement' />
              <Form.Input label='Unit Abbreviation:' placeholder='Unit Abbreviation' />
            <br/>
          <div className="center">
            <Button primary>Submit</Button>
          </div>
        </Form>
      </Segment>
    )
  }
}

export default UnitMeasurement;
