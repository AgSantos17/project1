import React, { Component } from 'react';
import { Form, Segment, Divider, Header, Button } from 'semantic-ui-react';

class Position extends Component
{
  render()
  {
    return(
      <Segment padded>
        <Form unstackable>
           <Divider horizontal><Header as='h1'>Maintenance: Position</Header></Divider>
            <br/>
          <Form.Group widths='equal'>
            <Form.Input label='Position Name:' placeholder='Position Name' />
          </Form.Group>
            <br/>
          <div className="center">
            <Button primary>Submit</Button>
          </div>
        </Form>
      </Segment>
    )
  }
}

export default Position;
