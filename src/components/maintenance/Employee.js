import PositionAPI from '../../data/position-api';
import React, { Component } from 'react';
import { Form, Segment, Divider, Header, Button } from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

const gender = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
]

class Employee extends Component
{

    constructor (props) {
    super(props)
    this.state = {
      startDate: moment()
    };
    this.handleChange = this.handleChange.bind(this);
    }

    handleChange(date) {
      this.setState({
        startDate: date
      });
    }

  render()
  {

    return(
      <Segment padded>
        <Form unstackable>
           <Divider horizontal><Header as='h1'>Maintenance: Employee</Header></Divider>
            <br/>
           <Divider horizontal><Header as='h3'>Employee Information</Header></Divider>
            <br/>
          <Form.Group widths='equal'>
            <Form.Input label='First Name:' placeholder='First Name' />
            <Form.Input label='Middle Name:' placeholder='Middle Name' />
            <Form.Input label='Last Name:' placeholder='Phone Number' />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input label='Birth Date:' control={DatePicker} selected={this.state.startDate} onChange={this.handleChange}
            peekNextMonth showMonthDropdown showYearDropdown maxDate={moment()} dropdownMode="select" width={5}/>
            <Form.Select label='Gender:' options={gender} placeholder='Gender' />
            <Form.Select label='Position:' options={PositionAPI.all()} placeholder='Position' />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input label='Contact Number:' placeholder='Contact Number' />
            <Form.Input label='Email Address:' placeholder='Email Address' />
          </Form.Group>
            <br/>
           <Divider horizontal><Header as='h3'>Account Information</Header></Divider>
            <br/>
          <Form.Group widths='equal'>
            <Form.Input label='Username:' placeholder='Username' />
            <Form.Input type="password" label='Password:' placeholder='Password' />
          </Form.Group>
            <br/>
           <Divider horizontal><Header as='h3'>Address</Header></Divider>
            <br/>
          <Form.Group widths='equal'>
            <Form.Input label='Street:' placeholder='Street' />
            <Form.Input label='City:' placeholder='City' />
          </Form.Group>
            <br/>
          <div className="center">
            <Button primary>Submit</Button>
          </div>
        </Form>
      </Segment>
    )
  }
}

export default Employee;
