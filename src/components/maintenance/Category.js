import React, { Component } from 'react';
import { Form, Segment, Divider, Header, Button } from 'semantic-ui-react';

class Category extends Component
{
  render()
  {
    return(
      <Segment padded>
        <Form unstackable>
           <Divider horizontal><Header as='h1'>Maintenance: Category</Header></Divider>
            <br/>
          <Form.Group widths='equal'>
            <Form.Input label='Category Name:' placeholder='Category Name' />
          </Form.Group>
            <br/>
          <div className="center">
            <Button primary>Submit</Button>
          </div>
        </Form>
      </Segment>
    )
  }
}

export default Category;
