import React, { Component } from 'react';
import { Form, Segment, Divider, Header, Button } from 'semantic-ui-react';

const category = [
  { key: '1', text: 'Metal', value: 'metal' },
  { key: '2', text: 'Wood', value: 'wood' },
]

class SubCategory extends Component
{
  render()
  {
    return(
      <Segment padded>
        <Form unstackable>
           <Divider horizontal><Header as='h1'>Maintenance: Sub-Category</Header></Divider>
            <br/>
              <Form.Select label='Category:' options={category} placeholder='Category' />
              <Form.Input label='Sub-Category Name:' placeholder='Category Name' />
            <br/>
          <div className="center">
            <Button primary>Submit</Button>
          </div>
        </Form>
      </Segment>
    )
  }
}

export default SubCategory;
