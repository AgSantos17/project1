import UmAPI from '../../data/unitM-api';
import CategoryAPI from '../../data/category-api';
import React, { Component } from 'react';
import { Form, Segment, Divider, Header, Button } from 'semantic-ui-react';
import 'react-datepicker/dist/react-datepicker.css';

class Materials extends Component
{

    constructor (props) {
    super(props)
    this.state = {
      value:' '
    }
    }

    handleChange = (e, { value }) => this.setState({ value })


  render()
  {
  const { value } = this.state
    return(
      <Segment padded>
        <Form unstackable>
           <Divider horizontal><Header as='h1'>Maintenance: Materials</Header></Divider>
            <br/>
           <Divider horizontal><Header as='h3'>Item Information</Header></Divider>
            <br/>
          <Form.Group widths='equal'>
            <Form.Select label='Category:'  onChange={this.handleChange} options={CategoryAPI.all()} placeholder='Category' width={8} />
            <Form.Input label='Measurement:' placeholder='Measurement' width={4} />
            <Form.Select label='Unit:' options={UmAPI.all()} placeholder='Unit Measurement' width={4} />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Select label='Sub-Category:' options={CategoryAPI.getSub(value)} placeholder='Sub-Category' />
            <Form.Input label='Brand:' placeholder='Brand'/>
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input label='Color:' placeholder='Color' />
            <Form.Input label='Package:' placeholder='Package' />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input label='Price:' placeholder='Price' />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.TextArea label='Description:' placeholder='Description...' />
          </Form.Group>
            <br/>
          <div className="center">
            <Button primary>Submit</Button>
          </div>
        </Form>
      </Segment>
    )
  }
}

export default Materials;
