import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import Home from './Home';
import Supplier from './maintenance/Supplier';
import Employee from './maintenance/Employee';
import Position from './maintenance/Position';
import Category from './maintenance/Category';
import SubCategory from './maintenance/SubCategory';
import UnitMeasurement from './maintenance/UnitMeasurement';
import Materials from './maintenance/Materials';
import Quotation from './transaction/Quotation/Quotation';
import Roster from './Roster';
import Schedule from './Schedule';
import App from './utilities/cart/components/App';


class Main extends Component {
  render() {
    return (
      <main>
      <Container fluid>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/supplier' component={Supplier}/>
          <Route path='/position' component={Position}/>
          <Route path='/employee' component={Employee}/>
          <Route path='/category' component={Category}/>
          <Route path='/sc' component={SubCategory}/>
          <Route path='/um' component={UnitMeasurement}/>
          <Route path='/materials' component={Materials}/>
          <Route path='/quotation' component={Quotation}/>
          <Route path='/roster' component={Roster}/>
          <Route path='/schedule' component={Schedule}/>
          <Route path='/cart' component={App}/>
        </Switch>
      </Container>
      </main>
    );
  }
}

export default Main;
