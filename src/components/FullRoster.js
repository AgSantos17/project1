// an API that returns a player object
import PlayerAPI from '../data/api';
import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import '../assets/style.css';
import matchSorter from 'match-sorter';

// Import React Table
import ReactTable from "react-table";

class FullRoster extends Component {

  handleInputChange = (e, { value }) => this.setState({ activePage: value })

  handlePaginationChange = (e, { activePage }) => this.setState({ activePage })

  render() {

    return (
        <ReactTable
          data={PlayerAPI.all()}
          filterable
          defaultFilterMethod={(filter, row) =>
            String(row[filter.id]) === filter.value}
          noDataText="No Data"
          columns={[
            {
              Header: "Player",
              columns: [
                {
                  Header: "#",
                  id: "number",
                  accessor: d => d.number
                },
                {
                  Header: "Name",
                  id: "name",
                  accessor: d => d.name,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["name"] }),
                  filterAll: true
                },
                {
                  Header: "Position",
                  id: "position",
                  accessor: d => d.position,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["position"] }),
                  filterAll: true
                },
                {
                  Header: 'Action',
                  id: "numbers",
                  accessor: 'number',
                    Cell: ({value}) => (<Button as={Link} to={`/Roster/${value}`}>View</Button>),
                  Filter: ({ filter, onChange }) =>
                  <div/>

                  }
              ]
            }
          ]}
          defaultPageSize={5}
          className="center"
        />
    )
  }
}

export default FullRoster;
