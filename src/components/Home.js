import React, { Component } from 'react';

class Home extends Component {

  render() {
    return (
      <div>
        <h1 className="center">Welcome to the Tornadoes Website!</h1>
      </div>
    );
  }
}

export default Home;
