// an API that returns a player object
import PlayerAPI from '../data/api'
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, Button, Container } from 'semantic-ui-react';

class Player extends Component {

  render() {
      const player = PlayerAPI.get(
      parseInt(this.props.match.params.number, 10)
      )
      if (!player) {
        return <div>Sorry, but the player was not found</div>
      }
    return (
      <div>
      <Container textAlign='center'>
         <Card className='fullWidth'>
          <Card.Content textAlign='center'>
            <Card.Header>
              {player.name}
            </Card.Header>
            <Card.Meta>
              <span className='date'>
                #{player.number}
              </span>
            </Card.Meta>
            <Card.Description>
              Position: {player.position}
            </Card.Description>
          </Card.Content>
          <Card.Content extra textAlign='center'>
            <Button as={Link} to="/Roster">Back</Button>
          </Card.Content>
        </Card>
     </Container>
    </div>
    );
  }
}

export default Player;
