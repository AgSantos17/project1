import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import FirstQuotation from './FirstQuotation';
import CreateQuotation from './CreateQuotation';
import ViewQuotation from './ViewQuotation';
import PrintQuotation from './PrintQuotation';

class Quotation extends Component {
  render() {
    return (
      <Switch>
        <Route exact path='/quotation' component={FirstQuotation}/>
        <Route path='/quotation/create/:key' component={CreateQuotation}/>
        <Route path='/quotation/view/:key' component={ViewQuotation}/>
        <Route path='/quotation/print/:key' component={PrintQuotation}/>
      </Switch>
    );
  }
}

export default Quotation;
