// an API that returns a player object
import QuotationAPI from '../../../data/quotation-api';
import React, { Component } from 'react';
import { Segment, Form, Divider, Header } from 'semantic-ui-react';
import '../../../assets/style.css';
import Cart from '../../utilities/cart/components/App';

class ViewQuotation extends Component {

  constructor() {
    super();
    this.state = {
      materials: [],
      totals:0,
    };
  }

  render() {
      const project = QuotationAPI.get(
      parseInt(this.props.match.params.key, 10)
      )

      if (!project) {
        return <div>Sorry, but the project was not found</div>
      }

    return (
      <div>
        <Segment>
      <Divider horizontal><Header as='h1'>Transaction: Quotation</Header></Divider>
        <br/><br/>
        <Form>
          <Form.Group >
            <Form.Input transparent label='Project:' placeholder='Project' value={project.project}/>
            <Form.Input transparent label='Address:' placeholder='Project' value={project.address}/>
          </Form.Group>
          <Form.Group >
            <Form.Input transparent label='Start Date:' value={project.date_start}/>
            <Form.Input transparent label='Due Date:' value={project.date_end}/>
          </Form.Group>
        </Form>

        <br/>
          <Cart />
        <br/><br/>
                  <Form >
                    <Form.Group >
                      <Form.Input transparent label='Prepared by:' placeholder='First Name' value={project.prepared}/>
                    </Form.Group>
                  </Form>
        </Segment>
    </div>
    );
  }
}

export default ViewQuotation;
