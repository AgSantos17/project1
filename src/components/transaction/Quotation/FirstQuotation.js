// an API that returns a player object
import QuotationAPI from '../../../data/quotation-api';
import React, { Component } from 'react';
import { Button, Segment, Form, Label, Divider, Header, Icon, Confirm } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import '../../../assets/style.css';
import matchSorter from 'match-sorter';
import moment from 'moment';

// Import React Table
import ReactTable from "react-table";

class FirstQuotation extends Component {

  state = { open: false, result: 'show the modal to capture a result' }

  show = () => this.setState({ open: true })
  handleConfirm = () => this.setState({ result: 'confirmed', open: false })
  handleCancel = () => this.setState({ result: 'cancelled', open: false })

  handleInputChange = (e, { value }) => this.setState({ activePage: value })

  handlePaginationChange = (e, { activePage }) => this.setState({ activePage })

  render() {

    return (
      <Segment>
    <Divider horizontal><Header as='h1'>Transaction: Quotation</Header></Divider>
        <Label as='a' color='teal' ribbon>Quotation Queue:<br/>{QuotationAPI.all().length+1}</Label>
      <br/><br/>
      <Form>
        <Form.Group >
          <Form.Input transparent label='Date:' placeholder='First Name' value={moment().format('L')}/>
        </Form.Group>
      </Form>

        <ReactTable
          data={QuotationAPI.all()}
          filterable
          defaultFilterMethod={(filter, row) =>
            String(row[filter.id]) === filter.value}
          noDataText="No Data"
          columns={[
            {
              Header: "Project",
              columns: [
                {
                  Header: "#",
                  id: "key",
                  accessor: d => d.key
                },
                {
                  Header: "Project",
                  id: "project",
                  accessor: d => d.project,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["project"] }),
                  filterAll: true
                },
                {
                  Header: "Customer",
                  id: "customer",
                  accessor: d => d.last_name +', '+ d.first_name+' '+ d.middle_name +' .',
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["customer"] }),
                  filterAll: true
                },
                {
                  Header: "Address",
                  id: "address",
                  accessor: d => d.address,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["address"] }),
                  filterAll: true
                },
                {
                  Header: "Date Started",
                  id: "date_start",
                  accessor: d => d.date_start,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["date_start"] }),
                  filterAll: true
                },
                {
                  Header: "Date Started",
                  id: "date_end",
                  accessor: d => d.date_end,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["date_end"] }),
                  filterAll: true
                },
                {
                  Header: "Prepared By",
                  id: "prepared",
                  accessor: d => d.prepared,
                  filterMethod: (filter, rows) =>
                    matchSorter(rows, filter.value, { keys: ["prepared"] }),
                  filterAll: true
                },
                {
                  Header: "Status",
                  id: "status",
                  accessor: d => d.status,
                  Filter: ({ filter, onChange }) =>
                  <div />
                }
              ]
            },
          {
            Header: "Action",
            columns: [
              {
                id: "key",
                accessor: "key",
                width: 250,
                  Cell: ({value,row}) => (
                <div>
                    {row.status==='active' ?
                      <Button.Group>
                      <Button size='small' color="teal" as={Link} to={`/quotation/view/${value}`}><Icon size='small' name='unhide' />Views</Button>
                      <Button.Or />
                      <Button size='small' color="green" as={Link} to={`/quotation/print/${value}`}><Icon size='small' name='print' />Print</Button>
                      </Button.Group>

                      :

                      <Button.Group>
                      <Button size='small' color="blue" as={Link} to={`/quotation/create/${value}`}><Icon size='small' name='check' />Accept</Button>
                      <Button.Or />
                      <Button size='small' onClick={this.show} color="red"><Icon size='small' name='trash' />Decline</Button>
                      </Button.Group>}
                </div>),
                Filter: ({ filter, onChange }) =>
                <div />
              },
            ]
            }
          ]}
          defaultPageSize={5}
          className="center"
        />

          <Confirm
            open={this.state.open}
            cancelButton='No!'
            confirmButton="Yes"
            content='Do you want to decline this request?'
            onCancel={this.handleCancel}
            onConfirm={this.handleConfirm}
            className="yes"
          />

        <br/><br/>
      </Segment>
    )
  }
}

export default FirstQuotation;
