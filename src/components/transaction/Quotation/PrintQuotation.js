import React from 'react';
import QuotationAPI from '../../../data/quotation-api';
import { Button, Segment, Form, Divider, Item, Table } from 'semantic-ui-react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import PdfQuotation from '../../utilities/pdf/PdfQuotation';

class PrintQuotation extends React.Component {

  render() {

    const project = QuotationAPI.get(
    parseInt(this.props.match.params.key, 10)
    )

    const total = project.item;
    if (!project) {
      return <div>Sorry, but the project was not found</div>
    }

    function sum(){
                var total =  0;
                for(var i=0;i<project.item.length;i++)
                  {
                    total = total + project['item'][i]['price'];
                   }

                 return total.toFixed(2);
                }

    return (
      <div>
      <Segment>
        <Segment>
        <div id="HTMLtoPDF" className="letter">
          <br/>
          <h2 className="center">Fareal Builders Inc</h2>
        <center>
         <p>1208 K8th., East Kamias</p>
         <p className="line-0">Quezon City</p>
        </center>
          <div className="right">
            Quotation no. <span className="bold underline">{project.key}</span>
          </div>
          <Divider className="bold-line"/>
            <center>
              <h1>Quotation</h1>
            </center>
          <Divider className="bold-line"/>
          <br/>
          <Item.Group>
            <Item>
              <Item.Content verticalAlign='middle' className="bold">Customer: <span className="no-bold"> {project.last_name +', '+ project.first_name+' '+ project.middle_name +' .'}</span></Item.Content>
              <Item.Content verticalAlign='middle' className="right bold">Date: <span className="no-bold"> {moment().format('L')}</span></Item.Content>
            </Item>

            <Item>
              <Item.Content verticalAlign='middle' className="bold">Project: <span className="no-bold"> {project.project}</span></Item.Content>
              <Item.Content verticalAlign='middle' className="right bold">Address: <span className="no-bold"> {project.address}</span></Item.Content>
            </Item>
          </Item.Group>
          <br/>
          <Table celled padded>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell singleLine>Brand</Table.HeaderCell>
                <Table.HeaderCell singleLine>Category</Table.HeaderCell>
                <Table.HeaderCell singleLine>Sub-category</Table.HeaderCell>
                <Table.HeaderCell singleLine>Description</Table.HeaderCell>
                <Table.HeaderCell singleLine>Quantity</Table.HeaderCell>
                <Table.HeaderCell singleLine>Price</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {
            total.map((item, index) =>
              <Table.Row key={index}>
                <Table.Cell>
                  {item.brand}
                </Table.Cell>
                <Table.Cell>
                  {item.category}
                </Table.Cell>
                <Table.Cell>
                  {item.subcategory}
                </Table.Cell>
                <Table.Cell>
                  {item.description}
                </Table.Cell>
                <Table.Cell>
                  {item.qty}
                </Table.Cell>
                <Table.Cell>
                  ₱ {item.price.toFixed(2)}
                </Table.Cell>
              </Table.Row>
                      )
                }
            </Table.Body>
          </Table>
          <Item.Group>
            <Item>
              <Item.Content verticalAlign='middle' className="center bold">Total: <span className="no-bold"> {'₱ '+sum()}</span></Item.Content>
            </Item>
          </Item.Group>
          <br/><br/><br/><br/><br/><br/>
          <Form>
            <Form.Group >
              <Form.Input transparent label='Prepared by :' placeholder='First Name' value={project.prepared}/>
            </Form.Group>
          </Form>
        </div>
         <br/>
         <PdfQuotation/>
      </Segment>
         <Button as={Link} to="/quotation">Back</Button>
    </Segment>
      </div>
    );
  }
}

export default PrintQuotation;
