// an API that returns a player object
import QuotationAPI from '../../../data/quotation-api';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Segment, Form, Divider, Header } from 'semantic-ui-react';
import matchSorter from 'match-sorter';
import '../../../assets/style.css';

// Import React Table
import ReactTable from "react-table";

class ViewQuotation extends Component {

  render() {
      const project = QuotationAPI.get(
      parseInt(this.props.match.params.key, 10)
      )

      if (!project) {
        return <div>Sorry, but the project was not found</div>
      }

      function sum(){
                  var total =  0;
                  for(var i=0;i<project.item.length;i++)
                    {
                      total = total + project['item'][i]['price'];
                      console.log(project);
                     }

                   return total.toFixed(2);
                  }

    return (
      <div>
        <Segment>
      <Divider horizontal><Header as='h1'>Transaction: Quotation</Header></Divider>
        <br/><br/>
        <Form>
          <Form.Group >
            <Form.Input transparent label='Project:' placeholder='Project' value={project.project}/>
            <Form.Input transparent label='Address:' placeholder='Project' value={project.address}/>
          </Form.Group>
          <Form.Group >
            <Form.Input transparent label='Start Date:' value={project.date_start}/>
            <Form.Input transparent label='Due Date:' value={project.date_end}/>
          </Form.Group>
        </Form>

          <ReactTable
            data={project.item}
            filterable
            defaultFilterMethod={(filter, row) =>
              String(row[filter.id]) === filter.value}
            noDataText="No Data"
            columns={[
              {
                Header: "Materials",
                columns: [
                  {
                    Header: "Brand",
                    id: "brand",
                    accessor: d => d.brand,
                    filterMethod: (filter, rows) =>
                      matchSorter(rows, filter.value, { keys: ["brand"] }),
                    filterAll: true
                  },
                  {
                    Header: "Category",
                    id: "category",
                    accessor: d => d.category,
                    filterMethod: (filter, rows) =>
                      matchSorter(rows, filter.value, { keys: ["category"] }),
                    filterAll: true
                  },
                  {
                    Header: "Sub-category",
                    id: "subcategory",
                    accessor: d => d.subcategory,
                    filterMethod: (filter, rows) =>
                      matchSorter(rows, filter.value, { keys: ["subcategory"] }),
                    filterAll: true
                  },
                  {
                    Header: "Description",
                    id: "description",
                    accessor: d => d.description,
                    filterMethod: (filter, rows) =>
                      matchSorter(rows, filter.value, { keys: ["description"] }),
                    filterAll: true
                  },
                  {
                    Header: "Color",
                    id: "color",
                    accessor: d => d.color,
                    filterMethod: (filter, rows) =>
                      matchSorter(rows, filter.value, { keys: ["color"] }),
                    filterAll: true
                  },
                  {
                    Header: "Package",
                    id: "package",
                    accessor: d => d.package,
                    filterMethod: (filter, rows) =>
                      matchSorter(rows, filter.value, { keys: ["package"] }),
                    filterAll: true
                  },
                  {
                    Header: "Measurement",
                    id: "measurement",
                    accessor: d => d.measurement,
                    filterMethod: (filter, rows) =>
                      matchSorter(rows, filter.value, { keys: ["measurement"] }),
                    filterAll: true
                  },
                  {
                    Header: "Quantity",
                    id: "qty",
                    accessor: d => d.qty,
                    filterMethod: (filter, rows) =>
                      matchSorter(rows, filter.value, { keys: ["qty"] }),
                    filterAll: true
                  },
                  {
                    Header: "Price",
                    id: "price",
                    accessor: d => '₱'+d.price,
                    filterMethod: (filter, rows) =>
                      matchSorter(rows, filter.value, { keys: ["price"] }),
                    filterAll: true
                  }
                ]
              }
            ]}
            defaultPageSize={5}
            className="center"
          />
          <br/><br/>

        <Form >
          <Form.Group >
            <Form.Input transparent label='Prepared by:' placeholder='First Name' value={project.prepared}/>
          </Form.Group>
          <Form.Field className="center">
            <label>Remaining balance:</label>
             <div className='ui transparent input'>
              <input className="center" placeholder='First Name' value={'₱'+sum()}/>
             </div>
          </Form.Field>
        </Form>

          <Button as={Link} to="/quotation">Back</Button>
        </Segment>
    </div>
    );
  }
}

export default ViewQuotation;
