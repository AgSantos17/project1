import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Menu, Container, Dropdown, Button } from 'semantic-ui-react';

class Header extends Component {

  state = {}

 handleItemClick = (e, { name }) => this.setState({ activeItem: name })

 render() {

   const { activeItem } = this.state;

    return (
      <header>

        <Menu inverted attached='top'>

         <Container>
          <Menu.Item as={Link} to="/" name='Home' active={activeItem === 'Home'} onClick={this.handleItemClick} />

          <Dropdown item text='Transaction'>
            <Dropdown.Menu>
              <Dropdown.Item as={Link} to="/quotation" name='Quotation' active={activeItem === 'Quotation'} onClick={this.handleItemClick}>Quotation</Dropdown.Item>
              <Dropdown.Item as={Link} to="/po" name='PurchaseO' active={activeItem === 'PurchaseO'} onClick={this.handleItemClick}>Purchase Order</Dropdown.Item>
              <Dropdown.Item as={Link} to="/delivery" name='Delivery' active={activeItem === 'Delivery'} onClick={this.handleItemClick}>Delivery</Dropdown.Item>
              <Dropdown.Item as={Link} to="/mr" name='MaterialR' active={activeItem === 'MaterialR'} onClick={this.handleItemClick}>Material Requisition</Dropdown.Item>
              <Dropdown.Item as={Link} to="/pullout" name='Pullout' active={activeItem === 'Pullout'} onClick={this.handleItemClick}>Pullout</Dropdown.Item>
              <Dropdown.Item as={Link} to="/billing" name='Billing' active={activeItem === 'Billing'} onClick={this.handleItemClick}>Billing</Dropdown.Item>
              <Dropdown.Item as={Link} to="/payment" name='Payment' active={activeItem === 'Payment'} onClick={this.handleItemClick}>Payment</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>

          <Dropdown item text='Maintenance'>
            <Dropdown.Menu>
              <Dropdown.Item as={Link} to="/supplier" name='Supplier' active={activeItem === 'Supplier'} onClick={this.handleItemClick} >Supplier</Dropdown.Item>
              <Dropdown item text='Employee'>
                <Dropdown.Menu>
                  <Dropdown.Item as={Link} to="/position" name='Position' active={activeItem === 'Position'} onClick={this.handleItemClick}>Position</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/employee" name='Employee' active={activeItem === 'Employee'} onClick={this.handleItemClick}>Employee</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
              <Dropdown item text='Materials'>
                <Dropdown.Menu>
                  <Dropdown.Item as={Link} to="/category" name='Category' active={activeItem === 'Category'} onClick={this.handleItemClick}>Category</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/sc" name='Sub-Category' active={activeItem === 'Sub-Category'} onClick={this.handleItemClick}>Sub-Categpry</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/um" name='UnitM' active={activeItem === 'UnitM'} onClick={this.handleItemClick}>Unit Measurement</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/materials" name='Materials' active={activeItem === 'Materials'} onClick={this.handleItemClick}>Materials</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Dropdown.Menu>
          </Dropdown>

          <Dropdown item text='Utilities'>
            <Dropdown.Menu>
                  <Dropdown.Item as={Link} to="/at" name='AuditT' active={activeItem === 'AuditT'} onClick={this.handleItemClick}>Audit Trail</Dropdown.Item>
              <Dropdown item text='Misc.'>
                <Dropdown.Menu>
                  <Dropdown.Item as={Link} to="/supplier" name='Supplier' active={activeItem === 'Home'} onClick={this.handleItemClick}>Supplier</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/supplier" name='Supplier' active={activeItem === 'Home'} onClick={this.handleItemClick}>Position</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/supplier" name='Supplier' active={activeItem === 'Home'} onClick={this.handleItemClick}>Employee</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/supplier" name='Supplier' active={activeItem === 'Home'} onClick={this.handleItemClick}>Category</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/supplier" name='Supplier' active={activeItem === 'Home'} onClick={this.handleItemClick}>Sub-Category</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/supplier" name='Supplier' active={activeItem === 'Home'} onClick={this.handleItemClick}>Unit Measurement</Dropdown.Item>
                  <Dropdown.Item as={Link} to="/supplier" name='Supplier' active={activeItem === 'Home'} onClick={this.handleItemClick}>Materials</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
                  <Dropdown.Item as={Link} to="/Roster" name='Roster' active={activeItem === 'Roster'} onClick={this.handleItemClick}>Inventory</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>

          <Menu.Menu position='right'>

            <Menu.Item as={Link} to="/Schedule" name='Calendar' active={activeItem === 'Schedule'} onClick={this.handleItemClick} />

              <Menu.Item>
                <Button primary>Sign Up</Button>
              </Menu.Item>

          </Menu.Menu>

         </Container>

        </Menu>

      </header>
    );
  }
}

export default Header;
