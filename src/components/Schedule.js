import React, { Component } from 'react';
import { Item } from 'semantic-ui-react';

class Schedule extends Component {

  render() {
    return (
        <Item.Group divided>
          <Item>
            <Item.Content verticalAlign='middle'>6/5 @ Evergreens</Item.Content>
          </Item>

          <Item>
            <Item.Content verticalAlign='middle'>6/8 vs Kickers</Item.Content>
          </Item>

          <Item>
            <Item.Content verticalAlign='middle'>6/14 @ United</Item.Content>
          </Item>
        </Item.Group>
    );
  }
}

export default Schedule;
