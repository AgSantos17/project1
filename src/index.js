import React from 'react';
import ReactDOM from 'react-dom';
import IndexPage from './components/IndexPage';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render((
  <BrowserRouter>
    <IndexPage />
  </BrowserRouter>
), document.getElementById('root'));
