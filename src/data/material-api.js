const MaterialApi = {
  material: [
    { key: 1,
    brand: "Mets",
    category: "Metals",
    subcategory: "Nails",
    description: "Wide",
    color: "Silver",
    package: "Box",
    measurement: "100mm",
    qty: 4,
    qtd: 1,
    price: 1000.70 },
    {  key: 2,
     brand: "Woodsky",
     category: "Wood",
     subcategory: "Stairs",
     description: "Solid",
     color: "Brown",
     package: "Box",
     measurement: "17ft",
     qty: 1,
     qtd: 1,
     price: 350.05 },
    {   key: 3,
      brand: "Meetal",
      category: "Metals",
      subcategory: "Hammer",
      description: "long",
      color: "Black",
      package: "Box",
      measurement: "12m",
      qty: 2,
      qtd: 1,
      price: 200.50 },
    {  key: 4,
     brand: "Eliks",
     category: "Electrics",
     subcategory: "Wires",
     description: "thin",
     color: "Black",
     package: "Box",
     measurement: "10ft",
     qty: 5,
     qtd: 1,
     price: 550.20 },
  ],

  all: function() { return this.material},

  get: function(id) {
    const isMaterial = m => m.number === id
    console.log(this.material);
    return this.material.find(isMaterial)
  }
}

export default MaterialApi;
