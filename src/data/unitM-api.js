// A simple data API that will be used to get the data for our
// components. On a real website, a more robust data fetching
// solution would be more appropriate.

const UmAPI = {
  Um: [
    { key: 1, text: "Centimeter", value: "Centimeter" },
    { key: 2, text: "Meter", value: "Meter" },
    { key: 3, text: "Kilometer", value: "Kilometer" },
    { key: 4, text: "Kilograms", value: "Kilograms" },
  ],
  all: function() { return this.Um },
  get: function(id) {
    const isUm = p => p.key === id
    return this.Um.find(isUm)
  }
}

export default UmAPI;
