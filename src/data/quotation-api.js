// A simple data API that will be used to get the data for our
// components. On a real website, a more robust data fetching
// solution would be more appropriate.

const QuotationAPI = {

      project:[{
        key: 1,
        project: "Fareal",
        address: "144 F.Roxas St., San Juan City",
        first_name: "John",
        middle_name: "H",
        last_name: "Doe",
        date_start: "03/14/2018",
        date_end: "11/21/2018",
        prepared: "Santos, Bettina R.",
        status: "active",
          item: [{
            key: 1,
            brand: "Mets",
            category: "Metals",
            subcategory: "Nails",
            description: "Wide",
            color: "Silver",
            package: "Box",
            measurement: "100mm",
            qty: 4,
            price: 100.70
                       },{
           key: 2,
           brand: "Woodsky",
           category: "Wood",
           subcategory: "Stairs",
           description: "Solid",
           color: "Brown",
           package: "Box",
           measurement: "17ft",
           qty: 1,
           price: 350.05
                       }],
        },{
          key: 2,
          project: "Condominum",
          address: "1 P.R. Sotto St., San Juan City",
          first_name: "Albert",
          middle_name: "k",
          last_name: "Heyu",
          date_start: "03/14/2018",
          date_end: "02/04/2019",
          prepared: "Santos, Antonio C.",
          status: "requested",
            item: [{
              key: 1,
              brand: "Meetal",
              category: "Metals",
              subcategory: "Hammer",
              description: "long",
              color: "Black",
              package: "Box",
              measurement: "12m",
              qty: 2,
              price: 200.50
                         },{
             key: 2,
             brand: "Eliks",
             category: "Electrics",
             subcategory: "Wires",
             description: "thin",
             color: "Black",
             package: "Box",
             measurement: "10ft",
             qty: 5,
             price: 550.20
                       }],
        }],

  all: function() { return this.project },

  getIndex: function(value) {
    const isUm = p => p.value === value
    return this.project.findIndex(isUm);
  },

  get: function(id) {
    const isUm = p => p.key === id
    console.log(this.project.find(isUm).item);
    return this.project.find(isUm)
  }

}

export default QuotationAPI;
