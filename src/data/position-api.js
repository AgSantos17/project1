// A simple data API that will be used to get the data for our
// components. On a real website, a more robust data fetching
// solution would be more appropriate.

const PositionAPI = {
  position: [
    { key: 1, text: "Admin", value: "Admin" },
    { key: 2, text: "Foreman", value: "Foreman" },
    { key: 3, text: "Quantity Surveyor", value: "Quantity Surveyor" },
    { key: 4, text: "Secretary", value: "Secretary" },
    { key: 5, text: "Stockman", value: "Stockman" },
    { key: 6, text: "Accountant", value: "Accountant" }
  ],
  all: function() { return this.position},
  get: function(id) {
    const isPosition = p => p.number === id
    return this.position.find(isPosition)
  }
}

export default PositionAPI;
