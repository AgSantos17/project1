// A simple data API that will be used to get the data for our
// components. On a real website, a more robust data fetching
// solution would be more appropriate.

const CategoryAPI = {

      category:[{
        key: 1,
        text: "Metal",
        value: "metal",
          subcategory: [{
            key: 1,
            text: "Stainless",
            value: "Stainless"
                       },{
            key: 2,
            text: "Solid",
            value: "Solid"
                       }],
        },{
        key: 2,
        text: "Metals",
        value: "metals",
          subcategory: [{
            key: 1,
            text: "Stainlesss",
            value: "Stainlesss"
                       },{
            key: 2,
            text: "Solids",
            value: "Solids"
                       }],
        }],

  all: function() { return this.category },

  getSub: function(value) {
    const isUm = p => p.value === value
    const isSC = this.category.findIndex(isUm)
    if(this.category[isSC]===undefined)
    {
      return
    }
      return this.category[isSC].subcategory

  },

  getIndex: function(value) {
    const isUm = p => p.value === value
    return this.category.findIndex(isUm);
  },

  get: function(id) {
    const isUm = p => p.key === id
    return this.category.find(isUm)
  }

}

export default CategoryAPI;
